from requests import get
import json

endpoint = "http://localhost:5000/api"

queries = [
    "what is coronavirus",
    "what is covid-19",
    "what are covid-19 symptoms",
    "what is coronavirus in latin",
    "is there a vaccine",
    "when covid-19 started",
    "how many people was estimated to need humanitarian assistance in 2020",
    "what will maximize the effectiveness of interventions",
    "what is possible in many resource-limitated settings",
    "why early identification of covid-19 patient can be difficult",
    "why early identification of patient is difficult",
    "what are the steps that a hospital should take after an outbreak",
    "when coronavirus disease discovered",
    "when covid-19 disease discovered",
    "what is estimate incubation period",
    "what was the effect of movement restriction policy on the Diamond Princess cruise ship",
    "what can be the main challenges in managing a hospital outbreak of covid-19",
    "how was the mouse-adapted SARS virus (M15) generated",
    "what were the findings in 2013",
    "where covid-19 identified",
    "What is SARS-CoV",
    "when the first case of covid-19 identified",
    "How is COVID-19 treated",
    "what is coronavirus treatment",
    "when the first case reported to World Health Organization (WHO)",
    "what is MERS",
    "how SARS-CoV and MERS-CoV are typfied",
    "what is SARS",
    "how the virus is spread",
    "which people are in highest risk",
    "what to do if i have COVID-19 symptoms",
    "what to do if i have COVID-19-like symptoms",
    "are dogs can transmit covid-19 to humans",
    "how To mitigate the impact of COVID-19",
    "what does WHO recommends",
    "who is coronavirus infect",
    "what is the primary goal In any medical surge",
    "what are the four transmission scenarios to describe the dynamic of the epidemic",
    "when SARS-CoV-2, identified for the first time",
    "how it can be transmitted",
    "how covid-19 can be transmitted",
    "when Coronaviruses were first described"
]

answers = list()

def ask(query):
    return get(endpoint, params={"query": query}).json()

for query in queries:
    response = ask(query)
    answers.append(response)

with open("./answers.json", "w+") as f:
    json.dump(answers, f)
