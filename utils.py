from cdqa.reader import BertProcessor, BertQA
from cdqa.utils.download import download_model
import joblib
import os

def init_reader():
    reader = BertQA(train_batch_size=12,
                    learning_rate=3e-5,
                    num_train_epochs=2,
                    do_lower_case=True,
                    output_dir='models')
    return reader

def train_reader(reader):
    train_processor = BertProcessor(do_lower_case=True, is_training=True)
    train_examples, train_features = train_processor.fit_transform(X='./training_data.json')
    reader.fit(X=(train_examples, train_features))
    return reader

def save_reader(reader, reader_name):
    joblib.dump(reader, os.path.join(reader.output_dir, reader_name))

def download_reader():
    """ Use Pretrained reader due to lack of resources """
    download_model(model='bert-squad_1.1', dir='./models')