# Covid-19 Question Answering

This project is a closed domain question answering web service, based on the data inside a collection of articles. using a module to sort articles according to the relevance to the givan question and a model based on Google's BertQA trained model.
This QA is about coronavirus, and we collected our articles from google scholar and other medical indexes


### how to run
  - ensure python 3.6 is installed on you'r computer
  - git clone https://gitlab.com/sami31/qa_answering.git
  - cd qa_answering
  - pip isntall -r requirements.txt
  - flask run -h 0.0.0.0

### using the GUI on loaclhost

  - use firefox browser
  - http://localhost:5000/api?query=<question\>

