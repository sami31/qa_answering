"""Import dependencies:"""
from cdqa.utils.converters import generate_squad_examples
from cdqa.pipeline import QAPipeline
from ast import literal_eval
# from cdqa.utils.converters import pdf_converter
from converter import convert_pdfs
from utils import download_reader
import pandas as pd
import os

""" import our code to train the reader """
# from .utils import init_reader, train_reader, save_reader

def init_pipeline():
    # uncomment if you want a train the reader:
    """ initialize a BertQA reader """
    # reader = init_reader()
    """ train it on our squad file"""
    # train_reader(reader)
    """ save it in models directory locally """
    # reader_name = "my_bert_qa_reader.joblib"
    # save_reader(reader, reader_name)
    """ if you want to load a local reader """
    # reader_dir = "./models/"
    # reader = joblib.load(reader_dir+reader_name)

    """ assume we alreadey downloaded the reader """
    """ and stored it in ./models/ directory """
    if not os.path.isfile('./models/bert_qa.joblib'):
        download_reader()
    reader = './models/bert_qa.joblib'

    """ initiailize pipeline with our reader and tfidf retrieve """
    cdqa_pipeline = QAPipeline(reader=reader, retriever="tfidf", retrieve_by_doc=False)

    """ reader fine tuning"""
    # cdqa_pipline.fit_reader("./training_data.json")

    """ fit retriever over our documents """
    pdf_dir = "./train_pdf.d/"
    if not os.path.isfile('./pdf_dataframe.csv'):
        df = convert_pdfs(pdf_dir)
        df.to_csv("pdf_dataframe.csv")
    else:
        df = pd.read_csv('./pdf_dataframe.csv', converters={"paragraphs": literal_eval})
    cdqa_pipeline.fit_retriever(df=df)

    return cdqa_pipeline

def predict(
        pipline,
        query: str = None,
        retriever_score_weight: float = 0.35
):

    if not isinstance(query, str):
        raise TypeError("invalid query type. please enter string.")

    retriever_prediction = pipline.retriever.predict(query)
    squad_examples = generate_squad_examples(
        question=query,
        best_idx_scores=retriever_prediction,
        metadata=pipline.metadata,
        retrieve_by_doc=False,
    )
    examples, features = pipline.processor_predict.fit_transform(X=squad_examples)
    prediction = pipline.reader.predict(
        X=(examples, features),
        n_predictions=None,
        retriever_score_weight=retriever_score_weight,
        return_all_preds=False,
    )
    return prediction

if __name__ == "__main__":

    pipline = init_pipeline()
    query = 'when corona started'
    prediction = pipline.predict(query)

    print('query: {}'.format(query))
    print('answer: {}'.format(prediction[0]))
    print('title: {}'.format(prediction[1]))
    print('paragraph: {}'.format(prediction[2]))
