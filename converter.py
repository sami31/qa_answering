import os
import re
import sys
from tika import parser
import pandas as pd

def convert_pdfs(directory_path, min_length=200):
    files = os.listdir(directory_path)
    pdfs = []
    for file in files:
        if file.endswith(".pdf"):
            pdfs.append(file)
    df = pd.DataFrame(columns=["title", "paragraphs"])
    for i, pdf in enumerate(pdfs):
        try:
            df.loc[i] = [pdf.replace(".pdf",''), None]
            raw = parser.from_file(os.path.join(directory_path, pdf))
            s = raw["content"].strip()
            paragraphs = re.split("\n\n(?=\u2028|[A-Z-0-9])", s)
            list_par = []
            for p in paragraphs:
                if "References" in p:
                    break
                if not p.isspace() and len(p) >= min_length:
                    if len(p) >= min_length:
                        print("paragraph: %s \nEND_PARAGRAPH - length: %s" % (p, len(p)))
                        list_par.append(p.replace("\n", ""))

            df.loc[i, "paragraphs"] = list_par
        except:
            print("Unexpected error:", sys.exc_info()[0])
            print("Unable to process file {}".format(pdf))
    return df

if __name__ == '__main__':
    pdf_dir = "./single_pdf.d/"
    df = convert_pdfs(pdf_dir, min_length=200)