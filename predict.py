from cdqa.utils.converters import generate_squad_examples

def predict(
        pipline,
        query: str = None,
        retriever_score_weight: float = 0.35
):

    if not isinstance(query, str):
        raise TypeError("invalid query type. please enter string.")

    retriever_prediction = pipline.retriever.predict(query)
    squad_examples = generate_squad_examples(
        question=query,
        best_idx_scores=retriever_prediction,
        metadata=pipline.metadata,
        retrieve_by_doc=False,
    )
    examples, features = pipline.processor_predict.fit_transform(X=squad_examples)
    prediction = pipline.reader.predict(
        X=(examples, features),
        n_predictions=1,
        retriever_score_weight=retriever_score_weight,
        return_all_preds=False,
    )
    return prediction