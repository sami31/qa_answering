from flask import Flask, request, jsonify
from flask_cors import CORS

from init_pipline import *

app = Flask(__name__)
CORS(app)

pipeline = init_pipeline()

@app.route("/api", methods=["GET"])
def api():
    query = request.args.get("query")
    print("query: ", query)
    #prediction = pipeline.predict(query=query)
    prediction = predict(pipeline, query)

    return jsonify(
        query=query, answer=prediction[0], title=prediction[1], paragraph=prediction[2]
    )